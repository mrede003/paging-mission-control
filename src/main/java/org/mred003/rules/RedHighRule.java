package org.mred003.rules;

import org.mred003.data.AbstractTelemetryEntryRule;
import org.mred003.data.TelemetryEntry;

/**
 * Rule implementation defining the criteria for a "Red High" rule
 * violation.
 *
 * @author mrede003
 */
public class RedHighRule extends AbstractTelemetryEntryRule {
    public RedHighRule(String component, int violationLimit, long range) {
        super("RED HIGH", component, violationLimit, range);
    }

    @Override
    public boolean isEntryViolation(TelemetryEntry entry) {
        return entry.getValue() > entry.getRedHighLimit();
    }
}
