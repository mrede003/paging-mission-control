package org.mred003.main;

import org.mred003.api.ITelemetryRulesProcessingService;
import org.mred003.data.AbstractTelemetryEntryRule;
import org.mred003.rules.RedHighRule;
import org.mred003.rules.RedLowRule;
import org.mred003.service.SingleIdTelemetryRulesProcessingService;
import org.mred003.utils.SerializationUtils;

import java.io.File;

/**
 * Entry point for Enlighten IT Paging Mission Control Challenge.
 *
 * @author matthew.redenius
 */
public class PagingMissionControlChallenge {
    public static void main(String[] args) {
        // If no argument.
        if (args.length < 1) {
            System.out.println("No arguments provided. Please provide a file and try again!.");
            return;
        }

        // If file doesn't exist.
        File entriesFile = new File(args[0]);
        if (!entriesFile.exists()) {
            System.out.println("File " + args[0] + " does not exist. Please provide a valid file and try again!");
            return;
        }

        // Init.
        ITelemetryRulesProcessingService telemetryRulesProcessingService = new SingleIdTelemetryRulesProcessingService();
        AbstractTelemetryEntryRule redLowRule = new RedLowRule("BATT", 3, 300000);
        AbstractTelemetryEntryRule redHighRule = new RedHighRule("TSTAT", 3, 300000);

        // Parse input file and add data.
        telemetryRulesProcessingService.putTelemetryData(SerializationUtils.deserializeTelemetryEntryFromFile(entriesFile));

        // Add Rules.
        telemetryRulesProcessingService.putRule(redLowRule);
        telemetryRulesProcessingService.putRule(redHighRule);

        // Process Data and serialize result to JSON.
        String jsonAlertStrings = SerializationUtils.serializeAlertsToJsonStringArray(telemetryRulesProcessingService.processData());

        // Output to console.
        System.out.println(jsonAlertStrings);
    }
}