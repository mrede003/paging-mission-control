package org.mred003.data;

import java.util.Date;
import java.util.Objects;

/**
 * POJO representing a telemetry alert.
 *
 * @author mrede003
 */
public class TelemetryAlert {

    private final int satelliteId;

    private final String severity;

    private final String component;

    private final Date timestamp;

    /**
     * @param satelliteId Satellite id the alert was generated from.
     * @param severity    Severity classifier of the alert.
     * @param component   Component that caused the alert.
     * @param timestamp   Timestamp the violation that caused the alert took place at.
     * @throws IllegalArgumentException if severity, component, or timestamp are null.
     */
    public TelemetryAlert(int satelliteId, String severity, String component, long timestamp) {
        if (severity == null || component == null) {
            throw new IllegalArgumentException("String severity or String component cannot be null");
        }
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = new Date(timestamp);
    }

    /**
     * Satellite id the alert was generated from.
     *
     * @return satelliteId int
     */
    public int getSatelliteId() {
        return satelliteId;
    }

    /**
     * Severity of the alert.
     *
     * @return severity {@link String}
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Component that caused the alert.
     *
     * @return component {@link String}
     */
    public String getComponent() {
        return component;
    }

    /**
     * Timestamp the violation causing the alert took place at.
     *
     * @return timestamp {@link Date}
     */
    public Date getTimestamp() {
        return timestamp;
    }


    @Override
    public String toString() {
        return "TelemetryAlert{" +
                "satelliteId='" + satelliteId + '\'' +
                ", severity='" + severity + '\'' +
                ", component='" + component + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TelemetryAlert)) return false;
        TelemetryAlert that = (TelemetryAlert) o;
        return Objects.equals(satelliteId, that.satelliteId) &&
                Objects.equals(severity, that.severity) &&
                Objects.equals(component, that.component) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }
}
