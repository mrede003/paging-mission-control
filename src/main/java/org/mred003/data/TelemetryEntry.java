package org.mred003.data;

import java.util.Objects;

/**
 * POJO representing a Telemetry Entry.
 *
 * @author mrede003
 */
public class TelemetryEntry {
    private final long timeStamp;

    private final int id;

    private final String component;

    private final float redHighLimit;

    private final float redLowLimit;

    private final float yellowHighLimit;

    private final float yellowLowLimit;

    private final float value;

    /**
     * @param timeStamp       Timestamp in millis the entry was generated at.
     * @param id              Id of satellite the entry was generated from.
     * @param component       Component that generated the entry.
     * @param redHighLimit    Upper red violation threshold.
     * @param redLowLimit     Lower red violation threshold.
     * @param yellowHighLimit Upper yellow violation threshold.
     * @param yellowLowLimit  Lower yellow violation threshold.
     * @param value           Value of the component.
     * @throws IllegalArgumentException if string component is null.
     */
    public TelemetryEntry(long timeStamp, int id, String component, float redHighLimit, float redLowLimit, float yellowHighLimit, float yellowLowLimit, float value) {
        if (component == null) {
            throw new IllegalArgumentException("String component cannot be null.");
        }
        this.timeStamp = timeStamp;
        this.id = id;
        this.component = component;
        this.redHighLimit = redHighLimit;
        this.redLowLimit = redLowLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.value = value;
    }

    /**
     * Timestamp in millis the entry was generated at.
     *
     * @return timestamp long
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * Id of satellite the entry was generated from.
     *
     * @return id int
     */
    public int getId() {
        return id;
    }

    /**
     * Component that generated the entry.
     *
     * @return component {@link String}
     */
    public String getComponent() {
        return component;
    }

    /**
     * Upper red violation threshold.
     *
     * @return redHighLimit float
     */
    public float getRedHighLimit() {
        return redHighLimit;
    }

    /**
     * Lower red violation threshold.
     *
     * @return redLowLimit float
     */
    public float getRedLowLimit() {
        return redLowLimit;
    }

    /**
     * Upper yellow violation threshold.
     *
     * @return yellowHighLimit float
     */
    public float getYellowHighLimit() {
        return yellowHighLimit;
    }

    /**
     * Lower yellow violation threshold.
     *
     * @return yellowLowLimit float
     */
    public float getYellowLowLimit() {
        return yellowLowLimit;
    }

    /**
     * Value of the component.
     *
     * @return value float
     */
    public float getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TelemetryEntry{" +
                "timeStamp=" + timeStamp +
                ", id=" + id +
                ", component='" + component + '\'' +
                ", redHighLimit=" + redHighLimit +
                ", redLowLimit=" + redLowLimit +
                ", yellowHighLimit=" + yellowHighLimit +
                ", yellowLowLimit=" + yellowLowLimit +
                ", value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TelemetryEntry)) return false;
        TelemetryEntry that = (TelemetryEntry) o;
        return timeStamp == that.timeStamp &&
                id == that.id &&
                Float.compare(that.redHighLimit, redHighLimit) == 0 &&
                Float.compare(that.redLowLimit, redLowLimit) == 0 &&
                Float.compare(that.yellowHighLimit, yellowHighLimit) == 0 &&
                Float.compare(that.yellowLowLimit, yellowLowLimit) == 0 &&
                Float.compare(that.value, value) == 0 &&
                component.equals(that.component);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeStamp, id, component, redHighLimit, redLowLimit, yellowHighLimit, yellowLowLimit, value);
    }
}
