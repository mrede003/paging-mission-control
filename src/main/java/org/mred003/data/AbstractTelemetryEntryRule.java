package org.mred003.data;

/**
 * Generic behavior associated with a rule to be applied to
 * {@link TelemetryEntry} objects.
 *
 * @author mrede003
 */
public abstract class AbstractTelemetryEntryRule {

    protected final int violationLimit;
    private final String severity;
    private final String component;
    private final long range;

    /**
     * Constructor.
     *
     * @param severity       Message representing the classifier of a rule violation.
     * @param component      Component this rule applies to.
     * @param violationLimit Number of violations that defines a rule
     *                       being broken.
     * @param range          Range of time in epoch millis a data set to be
     *                       checked for violations is constrained to.
     */
    public AbstractTelemetryEntryRule(String severity, String component, int violationLimit, long range) {
        this.severity = severity;
        this.component = component;
        this.violationLimit = violationLimit;
        this.range = range;
    }

    /**
     * API to determine if {@link TelemetryEntry} is in violation
     * of the rule implementation.
     *
     * @param entry The {@link TelemetryEntry} to be checked.
     * @return true if the {@link TelemetryEntry} is in violation
     * of the rule, false if otherwise.
     */
    public abstract boolean isEntryViolation(TelemetryEntry entry);

    /**
     * The number of violations that defines a rule being broken.
     *
     * @return int
     */
    public int getLimit() {
        return violationLimit;
    }

    /**
     * The range of time in epoch millis a data set checked for
     * violations is to be constrained to.
     *
     * @return int
     */
    public long getRange() {
        return range;
    }

    /**
     * Message representing the classifier of a rule violation.
     *
     * @return {@link String}
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Message representing the classifier of a rule violation.
     *
     * @return {@link String}
     */
    public String getComponent() {
        return component;
    }
}