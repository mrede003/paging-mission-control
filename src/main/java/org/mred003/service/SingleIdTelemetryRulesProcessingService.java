package org.mred003.service;

import org.mred003.api.ITelemetryRulesProcessingService;
import org.mred003.data.AbstractTelemetryEntryRule;
import org.mred003.data.TelemetryAlert;
import org.mred003.data.TelemetryEntry;

import java.util.*;

/**
 * Implementation of {@link ITelemetryRulesProcessingService} that processes {@link TelemetryEntry} data
 * against a provided set of {@link AbstractTelemetryEntryRule}s. This implementation verifies rules
 * against {@link TelemetryEntry}s of the same id and should not be used for rules applying to entries of
 * multiple ids.
 *
 * @author mrede003
 */
public class SingleIdTelemetryRulesProcessingService implements ITelemetryRulesProcessingService {

    /**
     * Map of telemetry entry ids to sorted telemetry entries.
     */
    private final Map<Integer, SortedSet<TelemetryEntry>> idToSortedEntries = new HashMap<>();

    /**
     * Set of rules to be applies to stored telemetry entries.
     */
    private final Set<AbstractTelemetryEntryRule> rules = new HashSet<>();

    @Override
    public boolean putTelemetryData(Set<TelemetryEntry> entries) {
        if (entries == null) {
            System.out.println("Cannot put null entry set. Fail fast.");
            return false;
        }
        // Filter by id since this implementation only deals with rules applying to the same id.
        entries.forEach(telemetryEntry -> {
            if (idToSortedEntries.containsKey(telemetryEntry.getId())) {
                idToSortedEntries.get(telemetryEntry.getId()).add(telemetryEntry);
            } else {
                // Sort by in ascending order by timestamp.
                SortedSet<TelemetryEntry> sortedEntries = new TreeSet<>(Comparator.comparingLong(TelemetryEntry::getTimeStamp));
                sortedEntries.add(telemetryEntry);
                idToSortedEntries.put(telemetryEntry.getId(), sortedEntries);
            }
        });
        return true;
    }

    @Override
    public boolean putRule(AbstractTelemetryEntryRule rule) {
        if (rule == null) {
            System.out.println("Cannot put null rule. Fail fast.");
            return false;
        }
        return rules.add(rule);
    }

    @Override
    public Set<TelemetryAlert> processData() {
        Set<TelemetryAlert> ruleViolationAlerts = new HashSet<>();
        // For each rule.
        rules.forEach(rule -> {
            // For each id.
            idToSortedEntries.forEach((key, value) -> {
                // If there are less values than what can constitute a rule violation.
                if (value.size() < rule.getLimit()) {
                    return;
                }
                List<TelemetryEntry> entriesList = new ArrayList<>(value);
                int front = 0;
                int lastIndex = entriesList.size() - 1;
                // Walk backwards until you have a set of data within range.
                for (int back = lastIndex; front < back; back--) {
                    if (isDataWithinRange(entriesList.get(front), entriesList.get(back), rule)) {
                        // Try to get violation from data set.
                        TelemetryEntry violation = getViolation(entriesList.subList(front, back + 1), rule);
                        // If there was a violation generate alert.
                        if (violation != null) {
                            ruleViolationAlerts.add(getAlertFromEntryAndRule(violation, rule));
                        }
                        // If at end of data set break, otherwise reset.
                        if (back == lastIndex) {
                            break;
                        } else {
                            back = lastIndex;
                            front++;
                        }
                    }
                }
            });
        });
        return ruleViolationAlerts;
    }

    /**
     * Helper function that returns the first {@link TelemetryEntry} that
     * violates the rule implementation in a list of entries. This
     * function is agnostic of ids associated with the given list of
     * entries such that it does verify id equality.
     *
     * @param entriesWithinRange The list of {@link TelemetryEntry} data to be checked.
     * @return {@link TelemetryEntry} the first violating entry in a list of entries.
     * Returns null if no rule violation is found.
     */
    private TelemetryEntry getViolation(List<TelemetryEntry> entriesWithinRange, AbstractTelemetryEntryRule rule) {
        // If the size of the data set is less than what violates a rule.
        if (entriesWithinRange.size() < rule.getLimit()) {
            return null;
        }
        // If data set is outside range of time.
        if (entriesWithinRange.get(entriesWithinRange.size() - 1).getTimeStamp() -
                entriesWithinRange.get(0).getTimeStamp() > rule.getRange()) {
            System.out.println("List of entry's time range is outside of rule definition. Returning null.");
            return null;
        }

        TelemetryEntry violation = null;
        int violations = 0;

        // Check if entry has defined rule component and violates rule.
        for (TelemetryEntry entry : entriesWithinRange) {
            if (isSameComponent(entry, rule) && rule.isEntryViolation(entry)) {
                violations++;
                // Only return the first violation even if the set contains more than violationLimit amount.
                if (violations == 1) {
                    violation = entry;
                } else if (violations == rule.getLimit()) {
                    return violation;
                }
            }
        }
        // No violation found.
        return null;
    }

    /**
     * Helper function to compare component equality between a given
     * {@link TelemetryEntry} and the rule implementation.
     *
     * @param entry {@link TelemetryEntry} The given entry.
     * @return true if the components are equal, false if otherwise.
     */
    private boolean isSameComponent(TelemetryEntry entry, AbstractTelemetryEntryRule rule) {
        return rule.getComponent().equals(entry.getComponent());
    }

    /**
     * Helper function to determine if two {@link TelemetryEntry}s are within a
     * {@link AbstractTelemetryEntryRule}'s range.
     *
     * @param entry1 {@link TelemetryEntry}
     * @param entry2 {@link TelemetryEntry}
     * @param rule   {@link AbstractTelemetryEntryRule}
     * @return true if the two entries are in range, false if otherwise.
     */
    private boolean isDataWithinRange(TelemetryEntry entry1, TelemetryEntry entry2, AbstractTelemetryEntryRule rule) {
        return (entry2.getTimeStamp() - entry1.getTimeStamp() <= rule.getRange());
    }

    /**
     * Helper function to generate a {@link TelemetryAlert} from a {@link AbstractTelemetryEntryRule}
     * and the violating {@link TelemetryEntry}.
     *
     * @param entry {@link TelemetryEntry}
     * @param rule  {@link AbstractTelemetryEntryRule}
     * @return {@link TelemetryAlert}
     */
    private TelemetryAlert getAlertFromEntryAndRule(TelemetryEntry entry, AbstractTelemetryEntryRule rule) {
        return new TelemetryAlert(entry.getId(), rule.getSeverity(), entry.getComponent(), entry.getTimeStamp());
    }
}
