package org.mred003.api;

import org.mred003.data.AbstractTelemetryEntryRule;
import org.mred003.data.TelemetryAlert;
import org.mred003.data.TelemetryEntry;

import java.util.Set;

/**
 * Service to allow for processing of {@link TelemetryEntry} data against
 * an internal rule set to produce {@link TelemetryAlert} data.
 *
 * @author mrede003
 */
public interface ITelemetryRulesProcessingService {

    /**
     * Puts a set {@link TelemetryEntry} into the internal data set.
     *
     * @param entries The set of {@link TelemetryEntry} to be put.
     */
    boolean putTelemetryData(Set<TelemetryEntry> entries);

    /**
     * Puts a {@link AbstractTelemetryEntryRule} into the internal data set.
     *
     * @param rule The {@link AbstractTelemetryEntryRule} to be put.
     */
    boolean putRule(AbstractTelemetryEntryRule rule);

    /**
     * Processes the internal set of {@link TelemetryEntry} data against
     * the internal set of {@link AbstractTelemetryEntryRule}s and returns a
     * set of {@link TelemetryAlert} data.
     *
     * @return A set of {@link TelemetryAlert} generated from rules being applied
     * to the internal data set.
     */
    Set<TelemetryAlert> processData();
}
