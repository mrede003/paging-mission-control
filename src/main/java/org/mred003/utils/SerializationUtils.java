package org.mred003.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.mred003.data.TelemetryAlert;
import org.mred003.data.TelemetryEntry;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;
import java.util.*;

/**
 * Utility functions for data serialization with {@link TelemetryEntry}s and {@link TelemetryAlert}s.
 *
 * @author mrede003
 */
public class SerializationUtils {

    /**
     * {@link Gson} instance for JSON serialization.
     */
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

    /**
     * Utility function to parse a file for {@link TelemetryEntry} data.
     * This function assumes valid input and does very little to handle invalid cases.
     * <p>
     * File format is <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>.
     *
     * @param file {@link File}
     * @return a set of {@link TelemetryEntry} data.
     */
    public static Set<TelemetryEntry> deserializeTelemetryEntryFromFile(File file) {
        HashSet<TelemetryEntry> entries = new HashSet<>();
        if (file == null) {
            System.out.println("File cannot be null. Returning empty set.");
            return entries;
        }
        try {
            Files.lines(file.toPath())
                    .map(String::trim)
                    .filter(s -> !s.isEmpty())
                    .forEach(s -> entries.add(getTelemetryEntry(s)));

        } catch (IOException e) {
            System.out.println("Could not parse file " +
                    file.toPath().toString() +
                    ".");
        }
        return entries;
    }

    /**
     * Utility function to serialize {@link TelemetryAlert} data to JSON {{@link String}}.
     * Alerts are sorted by id in ascending order.
     * {@link Date} is formatted as "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'".
     *
     * @param alerts The list of {@link TelemetryAlert} data.
     * @return {@link String} representing an array of JSON {@link TelemetryAlert}s.
     */
    public static String serializeAlertsToJsonStringArray(Set<TelemetryAlert> alerts) {
        if (alerts == null) {
            System.out.println("Alerts cannot be null. Returning empty list.");
            return "";
        }
        List<TelemetryAlert> sortedAlerts = new ArrayList<>(alerts);
        Collections.sort(sortedAlerts, Comparator.comparingInt(TelemetryAlert::getSatelliteId));
        return gson.toJson(alerts.toArray());
    }

    /**
     * Helper function to return {@link TelemetryEntry} from a {@link String}.
     *
     * @param input {@link String}
     * @return {@link TelemetryEntry}
     */
    private static TelemetryEntry getTelemetryEntry(String input) {
        String[] fields = input.split("\\|");
        if (8 != fields.length) {
            System.out.println("Entry " +
                    input +
                    " didn't have 8 fields. Returning null.");
            return null;
        }

        long timeStamp = Instant.from(new DateTimeFormatterBuilder()
                .appendPattern("yyyyMMdd HH:mm:ss.SSS")
                .toFormatter()
                .withZone(ZoneId.of("UTC"))
                .parse(fields[0])).toEpochMilli();

        int id = Integer.parseInt(fields[1]);
        float redHighLimit = Float.parseFloat(fields[2]);
        float yellowHighLimit = Float.parseFloat(fields[3]);
        float yellowLowLimit = Float.parseFloat(fields[4]);
        float redLowLimit = Float.parseFloat(fields[5]);
        float value = Float.parseFloat(fields[6]);
        String component = fields[7];
        return new TelemetryEntry(timeStamp, id, component, redHighLimit, redLowLimit, yellowHighLimit, yellowLowLimit, value);
    }
}
