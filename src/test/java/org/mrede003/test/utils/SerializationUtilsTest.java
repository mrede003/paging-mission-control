package org.mrede003.test.utils;

import org.junit.Assert;
import org.junit.Test;
import org.mred003.data.TelemetryEntry;
import org.mred003.utils.SerializationUtils;

import java.io.File;
import java.util.Set;

/**
 * Test class to verify utility functions related to {@link TelemetryEntry}.
 *
 * @author mrede003
 */
public class SerializationUtilsTest {

    // Test Data.
    private static final TelemetryEntry ENTRY_1 = new TelemetryEntry(1514847665001l, 1001, "TSTAT", 101.0f, 20.0f, 98.0f, 25.0f, 99.9f);
    private static final TelemetryEntry ENTRY_2 = new TelemetryEntry(1514847669521L, 1000, "BATT", 17.0f, 8.0f, 15.0f, 9.0f, 7.8f);
    private static final TelemetryEntry ENTRY_3 = new TelemetryEntry(1514847686011L, 1001, "TSTAT", 101.0f, 20.0f, 98.0f, 25.0f, 99.8f);
    private static final File INPUT_FILE = new File(SerializationUtilsTest.class.getClass().getResource("/testInput.txt").getFile());

    /**
     * Test function to verify de-serialization from file.
     */
    @Test
    public void deSerializeFromFileTest() {
        Set<TelemetryEntry> entries = SerializationUtils.deserializeTelemetryEntryFromFile(INPUT_FILE);
        Assert.assertTrue(entries.contains(ENTRY_1));
        Assert.assertTrue(entries.contains(ENTRY_2));
        Assert.assertTrue(entries.contains(ENTRY_3));
    }
}
