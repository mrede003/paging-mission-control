package org.mrede003.test.service;

import org.junit.Assert;
import org.junit.Test;
import org.mred003.api.ITelemetryRulesProcessingService;
import org.mred003.data.AbstractTelemetryEntryRule;
import org.mred003.data.TelemetryAlert;
import org.mred003.data.TelemetryEntry;
import org.mred003.rules.RedHighRule;
import org.mred003.rules.RedLowRule;
import org.mred003.service.SingleIdTelemetryRulesProcessingService;
import org.mred003.utils.SerializationUtils;
import org.mrede003.test.utils.SerializationUtilsTest;

import java.io.File;
import java.util.Set;

/**
 * Test class for {@link SingleIdTelemetryRulesProcessingService} implementation of {@link ITelemetryRulesProcessingService}.
 *
 * @author mrede003
 */
public class SingleIdTelemetryRulesProcessingServiceTest {
    // Test Data.
    private static final TelemetryAlert ALERT_1 = new TelemetryAlert(1000, "RED LOW", "BATT", 1514847669521l);
    private static final TelemetryAlert ALERT_2 = new TelemetryAlert(1000, "RED HIGH", "TSTAT", 1514847698001l);
    private static final File INPUT_FILE = new File(SerializationUtilsTest.class.getClass().getResource("/testInput.txt").getFile());

    /**
     * Test function to verify the {@link TelemetryAlert}s generated from a set of {@link TelemetryEntry}s;
     */
    @Test
    public void processRuleViolationsTest() {
        // Init.
        ITelemetryRulesProcessingService telemetryRulesProcessingService = new SingleIdTelemetryRulesProcessingService();
        AbstractTelemetryEntryRule redLowRule = new RedLowRule("BATT", 3, 300000);
        AbstractTelemetryEntryRule redHighRule = new RedHighRule("TSTAT", 3, 300000);

        // Parse input file and add data.
        telemetryRulesProcessingService.putTelemetryData(SerializationUtils.deserializeTelemetryEntryFromFile(INPUT_FILE));

        // Add Rules.
        telemetryRulesProcessingService.putRule(redLowRule);
        telemetryRulesProcessingService.putRule(redHighRule);

        // Generate resulting alert set.
        Set<TelemetryAlert> alerts = telemetryRulesProcessingService.processData();
        Assert.assertTrue(alerts.contains(ALERT_1));
        Assert.assertTrue(alerts.contains(ALERT_2));
    }
}
